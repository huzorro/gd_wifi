# Version: 0.0.1
FROM nginx
MAINTAINER ray "huzorro@gmail.com"
RUN mkdir -p /srv/wifi && ln -sf /usr/share/zoneinfo/Asia/Shanghai  /etc/localtime && rm -rf /etc/nginx/conf.d/default.conf
ADD nginx/wifi.conf /etc/nginx/conf.d/
ADD nginx/nginx.conf /etc/nginx/nginx.conf
ADD ./html/ /srv/wifi/
EXPOSE 50006 
